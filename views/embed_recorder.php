<?php

// No direct access
if (!defined('ABSPATH')) {
    die('Error #845173685');
}


/*
 * Get all recorded videos
 *
 * $limit - how many to pull at once
 * $offset - from which index should we start displaying
 */
$generatedShortcodes = $this->getGeneratedShortcodes();

?>
<style type="text/css">
    .settings-container {
        display: flex;
        flex-wrap: wrap;
        flex-flow: column;
        font-weight: bold;
    }
    .settings-body {
        padding-top: 15px;
        max-width: 750px;
        display: flex;
        flex-wrap: wrap;
        flex-flow: column;
    }
    td.comment {
        font-weight: normal;
        font-size: 12px;
        text-align: left;
    }
    .widefat td, .widefat th {
        vertical-align: middle;
    }
</style>
<div class="wrap">

    <?php if (!empty($this->accountHash)) { ?>
    <div class="settings-container">
        <h3><img src="<?php echo $this->getPluginViewURL('img/logo_big.png');?>" /> Embed the Pipe Video Recorder</h3>
        <div class="settings-body">

            <!-- Generate new shortcode -->
            <table class="widefat">
                <tr>
                    <th colspan="2" class="alternate"><strong>Shortcodes generator</strong></th>
                    <th class="alternate"></th>
                </tr>
                <tr>
                    <td>
                        <label for="videoLength">Video length</label>
                    </td>
                    <td>
                        <input id="videoLength" name="videoLength" type="text" style="width: 100%;" value="<?php echo $this->videoLength; ?>" />
                    </td>
                    <td class="comment">
                        How many <strong>seconds</strong> should a user be able to record himself?
                    </td>
                </tr>
                <tr class="alternate">
                    <td>
                        <label for="videoQuality">Video resolution</label>
                    </td>
                    <td>
                        <select id="videoQuality" style="width: 100%;">
                            <option value="240p" <?php echo ($this->videoQuality == '240p') ? 'selected' : '' ?>>240p - Small</option>
                            <option value="480p" <?php echo ($this->videoQuality == '480p') ? 'selected' : '' ?>>480p - Medium</option>
                            <option value="720p" <?php echo ($this->videoQuality == '720p') ? 'selected' : '' ?>>720p - Large</option>
                        </select>
                    </td>
                    <td class="comment" style="max-width: 250px;">
                        <strong>Small</strong>: 320 x 270 px | <strong>Medium</strong>: 640 x 510 px | <strong>Large</strong>: 640 x 390 px<br />
                        <hr />
                        <strong>Note:</strong> a high quality webcam is needed for <strong>720p</strong> and may not always be the best size for your regular users. The video quality is not necessarily tied up to the Video Resolution - you can obtain a very high quality video on the smallest resolution. It all depends on the web camera you or your users are using.
                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3" style="font-size: 12px; font-weight: normal;">
                        Create a unique video recorder profile which can be embedded with a <code>shortcode</code> in any page or post. This will allow registered users (members) or visitors to record themselves. Videos will be availabe both in the <strong>Recorded Videos</strong> plugin menu and in your <a href="https://addpipe.com" target="_blank">addpipe.com</a> account.
                    </td>
                </tr>
            </table>
            <p>
                <input class="button-primary" type="submit" name="save-settings" id="button-save-settings" value="Generate shortcode" />
            </p>

            <div style="margin-top: 35px;"></div>

            <!-- Available shortcodes -->
            <table class="widefat">
                <tr>
                    <th><strong>Generated shortcodes</strong></th>
                </tr>
                <?php

                if (empty($generatedShortcodes)) {
                    echo "
                            <tr>
                                <td>
                                    <h3>No shortcodes have been generated.</h3>
                                </td>
                            </tr>
                            ";
                } else {
                    foreach ($generatedShortcodes as $arr => $shortcode) {
                        echo "
                                <tr>
                                    <td>
                                        <input type='text' readonly value='[pipe recorder {$shortcode->shortcode_id}]' /><br />
                                        <small>
                                            <i class='dashicons dashicons-clock'></i>{$shortcode->video_length} seconds
                                            <i class='dashicons dashicons-admin-generic'></i> {$shortcode->video_quality}
                                        </small>
                                    </td>
                                </tr>
                        ";
                    }
                }
                ?>
            </table>

        </div><!-- End settings-body -->
    </div><!-- End settings-container -->

    <!-- Account hash check -->
    <?php
    } else {
        echo '
                <div class="error">
                    <h4>Account hash missing. Go to <a href="'.admin_url().'admin.php?page=addpipe-for-wordpress">Settings</a> and fill in your account hash. This can be obtained from <a href="https://addpipe.com" target="_blank">addpipe.com</a> - Account settings tab.</h4>
                </div>
                ';
    }
    ?>

</div><!-- End wrap -->


<script type="text/javascript">
    jQuery(document).ready(function($) {

        var settingsBody = $('.settings-body');
        var buttonSave = $('#button-save-settings');

        buttonSave.click(function() {

            // Mangle the DOM
            settingsBody.slideUp();
            settingsBody.after("<h3 id='message-saving-settings'>Generating shortcode...</h3>");
            buttonSave.hide();

            // Grab the data
            var videoLength = $('#videoLength').val();
            var videoQuality = $('#videoQuality option:selected').val();

            // Setup the data object
            data = {
                action: 'addpipe_ajax_shortcode_generator',
                length: videoLength,
                quality: videoQuality
            };

            // Post it and write the message
            setTimeout(function() {
                $.post(ajaxurl, data, function(response){
                    $('#message-saving-settings').html(response);
                });
            }, 1000);

            return false;
        });
    });
</script>




<?php
