<?php
/*
 * Plugin Name: Pipe Video Recorder
 * Plugin URI: https://addpipe.com
 * Description: Pipe Video Recorder allows you to place a 'record video' button on your website which can be used by visitors or registered members from both mobile and desktop browsers to record themselves. Pipe takes care of uploading, converting, publishing and storage of your videos. No more media servers and settings things up by yourself!
 * Author: addpipe.com
 * Author URI: https://addpipe.com
 * Developer: Lucian Alexandru
 * Developer URI: http://plainsight.ro
 * License: GPLv3
 * License URI: http://www.gnu.org/licenses/gpl-3.0
 * Text Domain: addpipe
 * Domain Path: /language
 * Network: false
 * Slug: addpipe
 * Version: 1.0.2
 */

/*
 * Load the main class
 */
require_once 'load/AddPipe.php';

/*
 * Register the install / uninstall hooks
 */
register_activation_hook(__FILE__, array('AddPipe', 'install'));
register_uninstall_hook(__FILE__, array('AddPipe', 'uninstall'));

/*
 * Run the plugin
 */
$addpipe = new AddPipe();
$addpipe->run();
