# Pipe integration for WordPress #

## Description ##

Ease the video recording and playback pain with Pipe. Our service handles the video recording and playback for both the web and mobile with all the different file formats while ensuring a secure storage & delivery through the Amazon CDN.

* Record from desktop and mobile - Pipe makes sure your website users can record video no matter the device and browser they're using.
* Play everywhere - different devices record to different video file formats including .mov, .mp4, .3gp and .flv. We make sure the final video is a proper .mp4 file that can be played on any device.
* Store on Amazon S3 or FTP - Pipe can easily push the end result (.mp4 video file) to your own Amazon S3 bucket or FTP for delivery.
* Easy To Integrate - Pipe comes with an easy to use JS Control API, a JS Events API and Webhooks.
* Up to 1080p HD Video - Pipe can record and properly manage videos up to 1080p (1920x1080) in resolution.
* Bandwidth independent - Pipe's client side and server side buffering make it possible to record high quality videos over slow or unstable connections like 3G/4G and public WiFi.

## Installation ##

1. In your WordPress backend (admin menu), go to `Plugins -> Add New` and search for `Pipe Video Recorder`
2. Click `Install`, then `Activate`. A new menu will be available: `Pipe Video Recorder`
3. In your WordPress backend menu (admin area), go to the `Pipe Video Recorder` -> `Settings` and enter your account hash.
Your account hash can be found in your addpipe.com account in the Account settings tab.
