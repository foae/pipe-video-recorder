<?php

// No direct access
if (!defined('ABSPATH')) {
    die('Error #845173685');
}

/*
 * Record new view
 */

?>
<style type="text/css">
    .go-green, .go-red, .go-yellow {
        display: inline-block;
        padding: 2px 4px;
        border-radius: 2px;;
        color: white;
        font-weight: bolder;
    }
    .go-green {
        background-color: green;
    }
    .go-red {
        background-color: red;
    }
    .go-yellow {
        background: darkgoldenrod;
    }
    .inside-centered {
        margin: auto;
        text-align: center;
    }
</style>

<div class="wrap">
    <h3><img src="<?php echo $this->getPluginViewURL('img/logo_big.png');?>" /> Record a new video</h3>
    <div id="poststuff">
        Record a new video and place it on any post or page using the embed code from the <a href="<?php echo admin_url() . 'admin.php?page=addpipe-for-wordpress/load/recorded-videos'; ?>" target="_self">list of recorded videos</a>.
        <div id="post-body" class="metabox-holder columns-2">
            <!-- main content -->
            <div id="post-body-content">
                <div class="meta-box-sortables ui-sortable">
                    <div class="postbox">
                        <div class="inside">
                            <div class="inside-centered">
                                <!-- The Pipe video recorder will be visible here -->
                            </div>
                        </div><!-- .inside -->
                    </div><!-- .postbox -->
                </div><!-- .meta-box-sortables .ui-sortable -->
            </div><!-- post-body-content -->

            <!-- sidebar -->
            <div id="postbox-container-1" class="postbox-container">
                <div class="meta-box-sortables">
                    <div class="postbox">
                        <h3>
                            <span>
                                Good to know
                            </span>
                        </h3>
                        <div class="inside">
                            <p>
                                The video will be made available in the <code>Recorded videos</code> menu and it can take from a few seconds up to a couple of minutes for it to be visible in your dashboard, depending on the video quality and the length you have selected.
                            </p>
                        </div><!-- .inside -->
                    </div><!-- .postbox -->
                    <div class="postbox">
                        <h3>
                            <span>
                                Your addpipe.com account status
                            </span>
                        </h3>
                        <div class="inside">
                            <p>Webhook: <span class='go-green'>OK</span> <?php //echo (method_exists($this, 'getRecordedVideos') && !empty($this->getRecordedVideos())) ? "<span class='go-green'>OK</span>" : "<span class='go-yellow'>None yet</span>"; ?></p>
                            <p>Account hash: <?php echo (!empty($this->accountHash)) ? "<span class='go-green'>OK</span>" : "<span class='go-red'>Missing</span>"; ?></p>
                            <a style="text-decoration: none; font-size: smaller;" href="https://addpipe.com/signin" target="_blank">Login into your account</a>
                        </div><!-- .inside -->
                    </div><!-- .postbox -->
                </div><!-- .meta-box-sortables -->
            </div><!-- #postbox-container-1 .postbox-container -->
        </div><!-- #post-body .metabox-holder .columns-2 -->
        <br class="clear">
    </div><!-- #poststuff -->
</div>


<style type="text/css">
    #accountHashCheck-missing {
        color: red;
        font-weight: bold;
        padding: 20px;
        margin-top: 20px;
        margin-bottom: 20px;
        background-color: white;
        border-radius: 15px 15px;
        text-align: center;
    }
</style>
<script type="text/javascript">

    var size = {
        width: <?php echo $this->videoWidth; ?>,
        height: <?php echo $this->videoHeight; ?>
    };

    var flashvars = {
        qualityurl: "avq/<?php echo $this->videoQuality; ?>.xml",
        accountHash: "<?php echo $this->accountHash; ?>",
        showMenu: "true",
        lang: "translations/en.xml",
        mrt: <?php echo $this->videoLength; ?>,
        payload: '<?php echo $this->accountHash . ',' . get_current_user_id() . ',' . $this->getThePostId(); ?>'
    };

    if (flashvars.accountHash.length < 10) {
        document.getElementsByClassName('inside-centered')[0].innerHTML += "<div id='accountHashCheck-missing'></div>";
        document.getElementById('accountHashCheck-missing').innerText = 'Missing account hash. You cannot Record or Play any videos. Please enter your account hash in the Settings menu';
    } else {
        document.getElementsByClassName('inside-centered')[0].innerHTML += "<div id='hdfvr-content'></div>";
    }

    (function () {
        var pipe = document.createElement('script');
        pipe.type = 'text/javascript';
        pipe.async = true;
        pipe.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 's1.addpipe.com/1.3/pipe.js';

        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(pipe, s);

    })();

</script>
