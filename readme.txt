=== Pipe Video Recorder ===
Contributors: lucian.alexandru, naicuoctavian
Donate link: https://addpipe.com/
Tags: video recording,video recorder,record video,vlog,record,wordpress video,webcam,camera,shortcode,clip,av,admin,comments,crowdsourced video,user generated content,video posts,video interviews,video reviews,video submission,video testimonial,video upload,user generated video,cam,recorder,recording,insert,page,video comments,video widget,video interview plugin,video submission plugin,video testimonial plugin,Post,posts,sidebar,social,media,users,video
Requires at least: 3.1
Tested up to: 4.3.1
Stable tag: 1.0.2
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.txt

Record video posts and accept video submissions on your mobile and desktop website using the new Pipe video recorder plugin for WordPress.

== Description ==

**IMPORTANT: You currently need a Pipe invite to use the plugin**. We approve new invites every 48 hours. You can request your free invite right now at [https://addpipe.com](https://addpipe.com).

**Use the Pipe Video Recorder to capture user generated video content** including video feedback, video submissions, video messages, and video testimonials straight form your WordPress website.

**Safely accept video resumes for e-recruiting** from any post or page on your website. The easiest way for online recruiters to collect video resumes.

**Transform your blog into a VLOG** using the backend video recording feature. Review your videos and post only those takes where you nailed it.

**Save time and money** since there's no more need for an expensive media server and everything happens automatically behind the scenes.

**Review and download recorded videos** straight from your WordPress backend.

**Embed recorded videos** in any web page or blog post (not necessarily on your website).

**Easily add crowdsourced videos to your website** and skyrocket your user engagement. You'll immediately start seeing the results.

Some of the features:

* Easy to install and configure plugin
* Works with any WordPress template
* Pipe works in the cloud, no media server (Red5, Wowza, AMS) needed
* Snapshots are created automatically for each video
* Record from desktop and mobile - Pipe makes sure your website users can record video no matter the device and browser they're using.
* Play everywhere - different devices record to different video file formats including .mov, .mp4, .3gp and .flv. We make sure the final video is a proper .mp4 file that can be played on any device.
* Up to 720p HD Video - Pipe can record and properly manage videos up to 720p (1280x720) in resolution if the user's webcam supports it.
* Bandwidth independent - Pipe's client side and server side buffering make it possible to record high quality videos over slow or unstable connections like 3G/4G and public WiFi.

A note on video file formats:

Pipe handles the video recording and playback process for both the web and mobile ensuring all the different video formats are converted to mp4 for ease of delivery as well as providing secure storage & delivery through the Amazon CDN.

== Installation ==

**Installing and setting up the plugin**

1. In your WordPress backend (admin menu), go to `Plugins` -> `Add New` and search for `Pipe Video Recorder`
2. Click `Install`, then `Activate`.
3. A new menu will new be available in the left sidebar `Pipe Video Recorder`. Click on it and go to `Settings`
4. Enter your addpipe.com **account hash** and click `[Save]`. Your account hash can be found in your https://addpipe.com account in the Account settings tab.
5. Go to My Account > Webhook on addpipe.com and enter the URL of your WordPress website.

**Connecting the plugin to your AddPipe.com account**

After you have saved the **account hash**, a Webhook link will be generated. Copy and navigate to your AddPipe.com account and paste it in the Webhook URL field. This will ensure proper communication between the WordPress plugin and AddPipe.com

**Recording your first video**

1. In your WordPress backend (admin menu), go to  `Pipe Video Recorder`->`Record New`
2. Record a video
3. Go to  `Pipe Video Recorder`->`Recorded Videos` to view, download or embed it

**Embedding a video recorder in a blog post or page**

1. In your WordPress backend (admin menu), go to `Plugins` -> `Embed Video Recorder`
2. Generate a shortcode using the desired video resolution and maximum length
3. Use the shortcode in a blog post or page
4. All recorded videos will show up in the `Recorded Videos` section of the plugin

== Screenshots ==

1. Plugin settings
2. Record a new video from the WP backend
3. View all recorded videos and play, download or embed them
4. Generating short codes for embedding the video recorder
5. Embedding the video recorder in a blog post using one of the generated short codes
6. Video recorder embedded in frontend (members and visitors can record themselves)

== Changelog ==

= 1.0.0 =
* First release

= 1.0.1 =
* Small bugfixes

= 1.0.2 =
* Menu creation patch and security enhancements
