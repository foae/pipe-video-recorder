<?php

// No direct access
if (!defined('ABSPATH')) {
    die('Error #845173685');
}

?>
<style type="text/css">
    .settings-container {
        display: flex;
        flex-wrap: wrap;
        flex-flow: column;
        font-weight: bold;
    }
    .settings-body {
        padding-top: 15px;
        max-width: 750px;
        display: flex;
        flex-wrap: wrap;
        flex-flow: column;
    }
    td.comment {
        font-weight: normal;
        font-size: 12px;
        text-align: left;
    }
    .widefat td {
        vertical-align: middle;
    }
</style>
<div class="wrap">
    <div class="settings-container">
        <h3><img src="<?php echo $this->getPluginViewURL('img/logo_big.png');?>" /> Pipe Video Recorder</h3>
        <div class="settings-body">

            <!-- General settings -->
            <table class="widefat">
                <tr>
                    <th colspan="2" class="alternate"><strong>My Pipe account settings</strong></th>
                    <th class="alternate"></th>
                </tr>
                <tr>
                    <td>
                        <label for="accountHash">
                            Account hash <br />
                            <?php echo (empty($this->accountHash)) ? "<span style='color: red;'>Missing</span>" : ''; ?>
                        </label>
                    </td>
                    <td>
                        <input id="accountHash" name="accountHash" type="text" style="width: 100%;" value="<?php echo $this->accountHash; ?>" />
                    </td>
                    <td class="comment">
                        <strong>Account hash</strong> can be found in your <strong>Account settings</strong> tab <br />from within your <a href="https://addpipe.com/signin" target="_blank">addpipe.com</a> account.<br />Don't have a Pipe account yet?<a href="http://addpipe.com/#saveEmailForm"> Claim your free invite here</a> (we usually approve new invite requests within 48 hours).
                    </td>
                </tr>
            </table>

            <br />

            <!-- Default settings -->
            <table class="widefat">
                <tr>
                    <th colspan="2" class="alternate"><strong>Default recorder settings</strong></th>
                    <th class="alternate"></th>
                </tr>
                <tr>
                    <td>
                        <label for="videoLength">Video length</label>
                    </td>
                    <td>
                        <input id="videoLength" name="videoLength" type="text" style="width: 100%;" value="<?php echo $this->videoLength; ?>" />
                    </td>
                    <td class="comment">
                        How many <strong>seconds</strong> should a user be able to record himself?
                    </td>
                </tr>
                <tr class="alternate">
                    <td>
                        <label for="videoQuality">Video resolution</label>
                    </td>
                    <td>
                        <select id="videoQuality" style="width: 100%;">
                            <option value="240p" <?php echo ($this->videoQuality == '240p') ? 'selected' : '' ?>>240p - Small</option>
                            <option value="480p" <?php echo ($this->videoQuality == '480p') ? 'selected' : '' ?>>480p - Medium</option>
                            <option value="720p" <?php echo ($this->videoQuality == '720p') ? 'selected' : '' ?>>720p - Large</option>
                        </select>
                    </td>
                    <td class="comment" style="max-width: 250px;">
                        <strong>Small</strong>: 320 x 270 px | <strong>Medium</strong>: 640 x 510 px | <strong>Large</strong>: 640 x 390 px<br />
                        <hr />
                        <strong>Note:</strong> a high quality webcam is needed for <strong>720p</strong> and may not always be the best size for your regular users. The video quality is not necessarily tied up to the Video Resolution - you can obtain a very high quality video on the smallest resolution. It all depends on the web camera you or your users are using.
                    </td>
                </tr>
            </table>
            <p colspan="3" style="font-size: small; font-weight: normal;">
                The video resolution and maximum video length will be used when you are <a href="<?php echo get_admin_url() . 'admin.php?page=addpipe-for-wordpress/load/record-new'; ?>">recording from backend</a>.
            </p>

        </div>
    </div>
    <p>
        <input class="button-primary" type="submit" name="save-settings" id="button-save-settings" value="Save" />
    </p>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){

        var settingsBody = $('.settings-body');
        var buttonSave = $('#button-save-settings');

        buttonSave.click(function(){

            // Mangle the DOM
            settingsBody.slideUp();
            settingsBody.after("<h3 id='message-saving-settings'>Saving new settings...</h3>");
            buttonSave.hide();

            // Grab the data
            var accountHash = $('#accountHash').val();
            var videoLength = $('#videoLength').val();
            var videoQuality = $('#videoQuality option:selected').val();

            // Setup the data object
            data = {
                action: 'addpipe_ajax_save_settings',
                accountHash: accountHash,
                videoLength: videoLength,
                videoQuality: videoQuality
            };

            // Post it and write the message
            setTimeout(function(){
                $.post(ajaxurl, data, function(response){
                    $('#message-saving-settings').html(response);
                });
            }, 500);

            return false;
        });
    });
</script>




<?php
